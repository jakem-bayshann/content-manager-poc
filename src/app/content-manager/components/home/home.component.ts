import { Component, OnInit, Directive, ViewContainerRef, ViewChild, ComponentFactoryResolver, TemplateRef, ComponentRef, Type, ElementRef } from '@angular/core';
import { dependencies } from 'src/client/dependencies';
import { CommonFeatureComponent } from '../common-feature/common-feature.component';
const availableFeatureComponents = [CommonFeatureComponent, ...dependencies.availableFeatureComponents, ];

function shuffle(a) {
  function attemptShuffle(a) {
    var b = JSON.parse(JSON.stringify(a));
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [b[i], b[j]] = [b[j], b[i]];
    }
    return b;
  }

  const original = a;
  var shuffled = a;
  var attempts = 0;

  while (JSON.stringify(original) === JSON.stringify(shuffled) && attempts < 20) {
    shuffled = attemptShuffle(original);
    attempts++;
  }

  return shuffled;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  options: string[];
  chosen: string[];

  toggle(option: string) {
    if (this.chosen.includes(option)) {
      this.chosen = this.chosen.filter(c => c !== option);
    } else {
      this.chosen = [ ...this.chosen, option ];
    }

    this.renderContent();
  }

  shuffleContent() {
    this.chosen = shuffle(this.chosen);
    this.renderContent();
  }

  @ViewChild('main', {read: ViewContainerRef}) mainContent: ViewContainerRef;
  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
    this.options = availableFeatureComponents.map(component => component.name);
    this.chosen = [];

    this.renderContent();
  }

  renderContent() {
    const viewContainerRef = this.mainContent;
    viewContainerRef.clear();

    const componentsToDisplay = this.chosen.map(key => availableFeatureComponents.find(component => component.name === key));

    componentsToDisplay.forEach(featureComponent => {
      const factory = this.componentFactoryResolver.resolveComponentFactory(featureComponent);
      viewContainerRef.createComponent(factory);
    });
  }

}
