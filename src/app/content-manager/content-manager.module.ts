import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { dependencies } from 'src/client/dependencies';
import { CommonFeatureComponent } from './components/common-feature/common-feature.component';

@NgModule({
  declarations: [HomeComponent, CommonFeatureComponent],
  imports: [
    CommonModule,
    dependencies.clientModule
  ],
  entryComponents: [CommonFeatureComponent],
  exports: [ HomeComponent ]
})
export class ContentManagerModule { }
