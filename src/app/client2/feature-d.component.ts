import { Component, OnInit } from '@angular/core';
import { IFeatureComponent } from '../content-manager/interfaces/IFeatureComponent';

@Component({
  selector: 'app-feature-d',
  template: `
    <p>
      feature-d works!
    </p>
  `,
  styles: []
})
export class FeatureDComponent implements OnInit, IFeatureComponent {
  alias: string = 'feature-c';

  constructor() { }

  ngOnInit() {
  }

}
