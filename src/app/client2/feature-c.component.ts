import { Component, OnInit } from '@angular/core';
import { IFeatureComponent } from '../content-manager/interfaces/IFeatureComponent';

@Component({
  selector: 'app-feature-c',
  template: `
    <p>
      feature-c works!
    </p>
  `,
  styles: []
})
export class FeatureCComponent implements OnInit, IFeatureComponent {
  alias: string = 'feature-c';

  constructor() { }

  ngOnInit() {
  }

}
