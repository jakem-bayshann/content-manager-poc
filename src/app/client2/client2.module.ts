import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { FeatureCComponent } from './feature-c.component';
import { FeatureDComponent } from './feature-d.component';

@NgModule({
  declarations: [LayoutComponent, FeatureCComponent, FeatureDComponent],
  imports: [
    CommonModule
  ],
  entryComponents: [FeatureCComponent, FeatureDComponent],
  exports: [LayoutComponent, FeatureCComponent, FeatureDComponent]
})
export class Client2Module { }
