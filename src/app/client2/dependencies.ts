import { LayoutComponent } from 'src/app/client2/layout/layout.component';
import { Client2Module } from 'src/app/client2/client2.module';
import { FeatureCComponent } from 'src/app/client2/feature-c.component';
import { FeatureDComponent } from 'src/app/client2/feature-d.component';

export const dependencies = {
    layoutComponent: LayoutComponent,
    clientModule: Client2Module,
    availableFeatureComponents: [ FeatureCComponent, FeatureDComponent ]
}