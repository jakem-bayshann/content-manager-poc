import { LayoutComponent } from 'src/app/client1/layout/layout.component';
import { Client1Module } from 'src/app/client1/client1.module';
import { FeatureAComponent } from 'src/app/client1/feature-a.component';
import { FeatureBComponent } from 'src/app/client1/feature-b.component';

export const dependencies = {
    layoutComponent: LayoutComponent,
    clientModule: Client1Module,
    availableFeatureComponents: [ FeatureAComponent, FeatureBComponent ]
}