import { Component, OnInit } from '@angular/core';
import { IFeatureComponent } from '../content-manager/interfaces/IFeatureComponent';

@Component({
  selector: 'app-feature-a',
  template: `
    <p>
      feature-a works!
    </p>
  `,
  styles: []
})
export class FeatureAComponent implements OnInit, IFeatureComponent {
  alias: string = 'feature-a';

  constructor() { }

  ngOnInit() {
  }

}
