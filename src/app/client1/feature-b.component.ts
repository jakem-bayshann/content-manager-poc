import { Component, OnInit } from '@angular/core';
import { IFeatureComponent } from '../content-manager/interfaces/IFeatureComponent';

@Component({
  selector: 'app-feature-b',
  template: `
    <p>
      feature-b works!
    </p>
  `,
  styles: []
})
export class FeatureBComponent implements OnInit, IFeatureComponent {
  alias: string = 'feature-b';

  constructor() { }

  ngOnInit() {
  }

}
