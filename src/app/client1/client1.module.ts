import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { SampleFeatureComponent } from './sample-feature/sample-feature.component';
import { FeatureAComponent } from './feature-a.component';
import { FeatureBComponent } from './feature-b.component';

@NgModule({
  declarations: [LayoutComponent, SampleFeatureComponent, FeatureAComponent, FeatureBComponent],
  imports: [
    CommonModule
  ],
  entryComponents: [SampleFeatureComponent, FeatureAComponent, FeatureBComponent],
  exports: [LayoutComponent, SampleFeatureComponent, FeatureAComponent, FeatureBComponent]
})
export class Client1Module { }
